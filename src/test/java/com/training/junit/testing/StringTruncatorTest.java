package com.training.junit.testing;

import static org.junit.Assert.*;

import org.junit.Test;

public class StringTruncatorTest {

	StringTruncator stringTruncator = new StringTruncator();
	
	@Test
	public void test() {
		
		assertEquals("BCD", stringTruncator.truncateFirst2AFromString("ABCD"));
	
	}
	@Test
	public void testForStringOfSize2() {
		
		String actual = stringTruncator.truncateFirst2AFromString("BC");
		String expected = "BC";
		assertEquals(expected, actual);
	
	}
	
	@Test
	public void testForStringOfSize2WithSameCharacters() {
		
		String actual = stringTruncator.truncateFirst2AFromString("AA");
		String expected = "";
		assertEquals(expected, actual);
	
	}
	@Test
	public void testForStringOfSize3WithSameCharacters() {
		
		String actual = stringTruncator.truncateFirst2AFromString("AAA");
		String expected = "A";
		assertEquals(expected, actual);
	
	}
	
	@Test
	public void testToCompareTwoHalfsOfString() {
		
		assertTrue("This is a Happy Path", stringTruncator.compareFirstHalfAndSecondHalfOfString("ABCD"));
	
	}


}
