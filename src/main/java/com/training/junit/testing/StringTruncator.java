package com.training.junit.testing;

public class StringTruncator {

	//AABC -> BC, ABCD -> ABCD, AA -> null, BC -> BC
	
	public String truncateFirst2AFromString(String str) {
		if(str.length() < 2)
			return str.replace("A", "");
		String first2Characters = str.substring(0, 2);
		String restOfTheString = str.substring(2);
				return first2Characters.replaceAll("A", "") + restOfTheString;
	}
	
	//ABCD (AB = CD) - > false, ABAB -> true
	public boolean compareFirstHalfAndSecondHalfOfString(String str) {//ABCD
		int length = str.length();//4
		String firstHalf = str.substring(0, length/2);//(0,2)
		String secondHalf = str.substring(length/2);
		
		return firstHalf.equals(secondHalf);
	}
}
